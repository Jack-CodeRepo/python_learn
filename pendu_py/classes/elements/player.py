#!/usr/bin/env python
# coding: utf-8
# ==================================================================================================


# ==================================================================================================
#   IMPORT
# ==================================================================================================



# ==================================================================================================
#   CLASSES
# ==================================================================================================



class class_player():
    def __init__(self, name: str = None, score: int = None):
        """
        class gérant le joueur en tant qu'objet


        :param name: nom du joueur, defaults to None
        :type name: str, optional
        :param score: score du joueur, defaults to None
        :type score: int, optional
        """
        
        self._name = name
        self._score = score

# --------------------------------------------------------------------------------------------------
    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value

# --------------------------------------------------------------------------------------------------
    @property
    def score(self):
        return self._score

    @score.setter
    def score(self, value):
        self._score = value


    def lower_score(self, value):
        self.score = int(self.score) - int(value)

    def increase_score(self, value):
        self.score = int(self.score) + int(value)