#!/usr/bin/env python
# coding: utf-8
# ==================================================================================================


# ==================================================================================================
#   IMPORT
# ==================================================================================================



# ==================================================================================================
#   CLASSES
# ==================================================================================================




class class_mot():
    def __init__(self, name: str = "", tentative: int = None):
        """
            class gérant le mot en tant qu'objet

            :param name: valeure du mot, defaults to None
            :type name: str, optional
            :param tentative: nombre de tentative pour trouver le mot, defaults to None
            :type tentative: int, optional
        """
        
        self._name = name
        self._tentative = tentative


# --------------------------------------------------------------------------------------------------
    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._name = value

# --------------------------------------------------------------------------------------------------
    @property
    def tentative(self):
        return self._tentative

    @tentative.setter
    def tentative(self, value):
        self._tentative = value


    def lower_tentative(self, value):
        self.tentative = int(self.tentative) - int(value)