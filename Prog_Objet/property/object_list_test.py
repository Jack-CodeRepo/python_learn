#!/usr/bin/env python
# coding: utf-8
# ==================================================================================================



# ==================================================================================================
#   IMPORT
# ==================================================================================================

import random



# ==================================================================================================
#   CLASS
# ==================================================================================================

class monster_type:
    # skills
    s_bite = 0

    # element
    e_poison = 0



    def __init__(self, monster_ref=None):
        self._monster_ref = monster_ref


        if "snake" in self._monster_ref:
            self.init_snake()
            print("init snake")

        else:
            self.init_default()
            print("init default")


# --------------------------------------------------------------------------------------------------
#   Properties
# --------------------------------------------------------------------------------------------------
    @property
    def monster_ref(self):
        return self._monster_ref

    @monster_ref.setter
    def monster_ref(self, value):
        self._monster_ref = value

# --------------------------------------------------------------------------------------------------
    @property
    def monster_type(self):
        return self._monster_type

    @monster_type.setter
    def monster_type(self, value):
        self._monster_type = value

# --------------------------------------------------------------------------------------------------
    @property
    def monster_name(self):
        return self._monster_name

    @monster_name.setter
    def monster_name(self, value):
        self._monster_name = value

# --------------------------------------------------------------------------------------------------
    @property
    def bite(self):
        return self.s_bite

    @bite.setter
    def bite(self, value):
        self.s_bite = value

# --------------------------------------------------------------------------------------------------
    @property
    def poison(self):
        return self.e_poison

    @poison.setter
    def poison(self, value):
        self.e_poison = value



# ==================================================================================================
#   fonction d'initialisation selon type de monstre
# ==================================================================================================

    def init_snake(self):
        self._monster_type =        "snake"
        self.s_bite =               random.randint(1, 10)
        self.e_poison =             random.randint(11, 25)

    def init_default(self):
        self._monster_type =        "default"
        self.s_bite =               random.randint(1, 10)




# ==================================================================================================
#   SCRIPT
# ==================================================================================================

# code to create up to 10 object and add it in obj_list
obj_list = []
for i, n in enumerate(range(0, 10)):
    if n % 2 == 0:
        monst = monster_type(monster_ref=f"snake_{i}")
    else:
        monst = monster_type(monster_ref=f"default_{i}")
    obj_list.append(monst)


indexMin = 2
# print same attribute of each object stored in obj_list
for i, obj in enumerate(obj_list[indexMin:]):
    print("for monster type ", obj.monster_type," stored in obj_list at index ", i, " 'bite' attribute is", obj.bite, " and its ref is ", obj.monster_ref)
print()


# pick a random object from obj_list
random_object = obj_list[random.randrange(len(obj_list))]
print("for random_object picked from obj_list type is ", random_object.monster_type," 'bite' attribute is ", random_object.bite, " and its ref is ", random_object.monster_ref)
print("for random_object picked from obj_list type is ", random_object.monster_type," 'poison' attribute is ", random_object.poison, " and its ref is ", random_object.monster_ref)
print()


# pick a specified object from obj_list with index
index = 2
precise_object = obj_list[index]
print("for precise_object picked from obj_list at index", index," type is ", precise_object.monster_type," bite attribute is ", precise_object.bite, " and its ref is ", precise_object.monster_ref)
print("for precise_object picked from obj_list at index", index," type is ", precise_object.monster_type," poison attribute is ", precise_object.poison, " and its ref is ", precise_object.monster_ref)
print()
