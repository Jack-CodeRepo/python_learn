#!/usr/bin/env python
# coding: utf-8
# ==================================================================================================



# ==================================================================================================
#   IMPORT
# ==================================================================================================

import json



# ==================================================================================================
#   VARIABLES GLOBALES
# ==================================================================================================
save_file="test.json"

player = "jacques"
new_player = "Bob".lower()

with open(save_file) as f:
    data = json.load(f)



# ==================================================================================================
#   FONCTIONS
# ==================================================================================================

def write_json(myData):
    with open(save_file, "w") as f:
        json.dump(myData, f, indent=4)



def check_player(player, myData):
    if player in myData["players"]:
        score = myData["players"][player]
        return "{} is {}".format(player, score)
    else:
        return f"Player {player} not found"



def change_score(player, score, myData):
    if player in myData["players"]:
        myData["players"][player] = score
    write_json(myData)



def add_player(player, myData):
    pName = player.lower()
    if pName not in myData["players"]:
        score = "10"
        temp = myData["players"]
        added_data = {player: score}
        temp.update(added_data)
        write_json(myData)
    else:
        print("Player already exists")



def delete_player(player, myData):
    pName = player.lower()
    if pName in myData["players"]:
        del myData["players"][pName]
        write_json(myData)



# ==================================================================================================
#   SCRIPT
# ==================================================================================================

print(f"Checking if {player} is in file")
print(check_player(player, data))
print(f"Changing {player} score and showing it")
change_score(player, "10", data)
print(check_player(player, data))
change_score(player, "1", data)     # reset data value



print()
print(f"Checking if {new_player} is in file")
print(check_player(new_player, data))
print(f"Adding {new_player} in file")
add_player(new_player, data)
print(f"Checking if {new_player} is in file")
print(check_player(new_player, data))


print()
print(f"Deleting {new_player} from file")
delete_player(new_player, data)
print(f"Checking if {new_player} is in file")
print(check_player(new_player, data))
