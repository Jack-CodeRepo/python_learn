from random import randint
from datetime import date, datetime
import json


def check_tuple(my_tuple=None, my_elem=None) -> bool:
    if type(my_tuple) is not tuple:         # même effet que     if type(my_tuple) != tuple:
        print(f"arg 'my_tuple' is not a tuple :: type is {type(my_tuple)}")
        exit()
    if my_elem in my_tuple:
        return True
    return False

def calculate_age_in_years(year: int=2):
    return int(str(date.today()).split("-")[0]) - year

def roll_dice(nb_des=1, max_value=6):
    stat = int()
    for de in range(nb_des):
        stat += randint(0, max_value)
    return stat

def set_pv(min, max, nb_dice=2, dice_value=20):
    a = roll_dice(nb_dice,dice_value)
    while min < a > max:
        a = roll_dice(nb_dice,dice_value)
    return a


def read_json_file(filepath):
    with open(filepath, 'r') as f:
        data = json.load(f)
    return data

def write_data_json_file(data, filepath):
    with open(filepath, 'w') as f:
        json.dump(data, f, indent=4)

def save_personnages(dico_des_persos):
    write_data_json_file(dico_des_persos, 'sauvegarde.json')


if __name__ == "__main__":
    print(roll_dice(1,6))