from src.algos.algo import roll_dice


class Personnage:
    def __init__(self, name: str, *args ,**Kwargs):
        self.nom = name
        self.taille = Kwargs.get("taille", roll_dice(2,80))
        self.force = Kwargs.get("force", roll_dice(2,20))
        self.age = Kwargs.get("age", roll_dice(2,20))
        self.vivant = True
        self.pv = Kwargs.get("pv", roll_dice(2,20))


    def deplacer(self):
        print(f"{self.nom} s'avance")

    def level_up(self, force):
        self.force += force

    def mourir(self):
        self.vivant = False
        self.pv = 0
        print(f"{self.nom} il est mouru !")
        exit()

    def attaquer(self, target) -> None:
        """
        :param target: obj héritant de la classe personnage
        :return: None

        @self attack target
        """
        target.pv -= self.force
        self.check_pv()
        target.check_pv()
        print(f"{self.nom} attaque  {target.nom} et fait {self.force} de dégats")
        print(f"il reste {target.pv} PV à {target.nom}")



    def check_pv(self):
        if self.pv <= 0:
            self.mourir()

    def manger(self):
        print("Cela n'a aucun effet.")

    def dormir(self):
        if self.pv < 100: self.pv += 30
        if self.pv > 100: self.pv = 100


    def __str__(self):
        return f"""\nAttributs du personnage:
        nom     = {self.nom}
        taille  = {self.taille}
        force   = {self.force}
        age     = {self.age}
        vivant  = {self.vivant}
        pv      = {self.pv}
        """

    def get_data(self):
        return self.__dict__