from .elfe import Elfe

class NightElfe(Elfe):
    def __init__(self,*args,**Kwargs):
        super().__init__(*args,**Kwargs)
        self.night_vision=True

    def vision_nuit(self):
        if self.night_vision:
            print(f"{self.nom} vois la nuit")
        print(f"{self.nom} ne vois rien la nuit #bigleu")