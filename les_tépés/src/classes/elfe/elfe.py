from src.classes.personnage import Personnage
from src.algos.algo import set_pv


class Elfe(Personnage):
    def __init__(self, *args, **Kwargs):
        super().__init__(*args, **Kwargs)
        self.pv =  set_pv(1000,2000,5,500)
        self.night_vision = False
        self.weapon = ["bow", "dagger"]


    def attaquer(self,*args) -> None:
        super().attaquer(*args)
        print("tirer à l'arc")

    def deplacer(self):
        print(f"{self.nom} ne marche pas, il danse !")