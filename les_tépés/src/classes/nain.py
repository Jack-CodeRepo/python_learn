# from src.classes.personnage import Personnage
from src.algos.algo import roll_dice

from .personnage import Personnage

class Nain(Personnage):
    def __init__(self, *args, **Kwargs):
        super().__init__(*args, **Kwargs)
        self.weapon = ["axe", "threats", "insults"]
        self.rancunier = True
        self.taille = roll_dice(2,20)+50

    def deplacer(self):
        print(f"{self.nom} ne marche pas, il sprint !")
