
# add src to path
from os import path as op
from sys import path as sp
sp.insert(0, op.abspath(op.join(op.dirname(__file__), '..')))


# import required functions and classes from src

from src.classes.nain import Nain
from src.classes.elfe.night_elfe import NightElfe
from src.algos.algo import roll_dice, calculate_age_in_years
from src.algos.algo import write_data_json_file
