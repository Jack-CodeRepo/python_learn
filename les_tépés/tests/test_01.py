
from context import *

grincheux = Nain("grincheux", pv=200)
legolas = NightElfe("Legolas")

print(legolas.get_data())
print(grincheux.get_data())

data = {legolas.nom: legolas.get_data(), grincheux.nom: grincheux.get_data()}
write_data_json_file(data, 'data.json')

