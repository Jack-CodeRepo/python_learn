#!/usr/bin/env python
# coding: utf-8
# ==================================================================================================

from sys import argv

def bin_conv(my_int: int):
    final_number = []
    my_int = int(my_int)
    while my_int != 0:
        print(my_int, " - ", my_int%2)
        final_number.insert(0, str(my_int%2))
        my_int = my_int // 2
    return "".join(final_number)

if __name__ == "__main__":
    print(bin_conv(argv[1]))


