#!/usr/bin/env python
# coding: utf-8
# ==================================================================================================

def recu(x):
    print(f"arg x is: {x}")
    if x == 1:
        return 1
    else:
        return x * recu(x - 1)

print(f"recursivite de 2: {recu(3)}")
print('======================================================================')
print(f"recursivite de 5: {recu(5)}")