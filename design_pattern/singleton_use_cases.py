
from design_patterns import MetaSingleton

class mon_thon_celibatard(metaclass = MetaSingleton):
    def __init__(self, a_name) -> None:
        self.__name = a_name

    def __str__(self) -> str:
        return "mon_thon_celibatard // name:{self.name}"

    @property
    def name(self):
        return self.__name
    @name.setter
    def name(self, value):
        self.__name = value



if "__main__" in __name__:
    tuna       = mon_thon_celibatard("jean")
    rien        = mon_thon_celibatard("jacques")
    compris     = mon_thon_celibatard("arnaud")

    print("What is called:\n\
    truna       = mon_thon_celibatard('jean')\n\
    rien        = mon_thon_celibatard('jacques')\n\
    compris     = mon_thon_celibatard('arnaud')\n\
    ")

    print("What is checcked:\n\
    if tuna is rien and tuna is compris\n\
    ")

    print(tuna.__repr__(), "    ", f"attr 'name' value is: {tuna.name}")
    print(rien.__repr__(), "    ", f"attr 'name' value is: {rien.name}")
    print(compris.__repr__(), "    ", f"attr 'name' value is: {compris.name}")
    print()

    if tuna is rien and tuna is compris:
        print("Result is:\n\
    All instances have same memory adress.\
    'MetaSingleton metaclass is successfull\
    ")

    MetaSingleton.list_all_instances(MetaSingleton)