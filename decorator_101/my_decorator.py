
from time import time, sleep



def time_estimate(func):
    def wrapper(*args, **kwargs):
        start_time = time()
        print("function as parameter is: ",func.__name__)
        func(*args, **kwargs)
        exec_time = time() - start_time
        print(f"execution time is {exec_time}")

    return wrapper

def dec_with_arg(num):
    def real_decorator(func):
        def wrapper(*args, **kwargs):
            for r in range(num):
                func(*args, **kwargs)
        return wrapper
    return real_decorator