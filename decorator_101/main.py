
from my_decorator import time_estimate, sleep, dec_with_arg



@time_estimate
def new_func(oui, *args):
    print(f"{oui} {args} hello sausage")
    sleep(1)

@dec_with_arg(6)
def nouvelle_fonction(oui, *args):
    print(f"{oui} {args} bonjour saucisse")

