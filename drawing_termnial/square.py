from random import randrange
from sys import argv

width = 10
heigth = 10

border_char = chr(randrange(250, 500))
fill_char = chr(randrange(20, 250))


for w in range(0, width + 1):
    for h in range(0, heigth + 1):
        if w >= 1 and w < width and h >=1 and h < heigth:
            print(fill_char, end=" ")
        else:   print(border_char, end=" ")
    print()