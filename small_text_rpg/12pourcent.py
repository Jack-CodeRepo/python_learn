#!/usr/bin/env python
# coding: utf-8
# ==================================================================================================

import json

# ==================================================================================================
#    VARIABLES
# ==================================================================================================

game = True

# ==================================================================================================
#    LISTS & DICTIONNARIES
# ==================================================================================================

with open("data_hist.json", "r") as f:
    data = json.load(f)

# ==================================================================================================
#    FUNCTIONS
# ==================================================================================================

def get_answer():
    while True:
        r = input("Alors ? 1 ou 2 ?: ")
        if(r == "1" or r == "2"):
            print(" --- ")
            return int(r)
        else:
            print("Vous n'avez pas répondu correctement. Vous devez choisir 1 ou 2")


def print_text(my_string_list):
    for string in my_string_list:
        print(string)


def get_data(target_key):
    for num_idx in data:
        if data[num_idx]["origin"] == target_key:
            return data[num_idx]


# ==================================================================================================
#    SCRIPT
# ==================================================================================================

print_text(get_data("intro")["string_list"])
cur_game_data = get_data("intro")
# tgt_game_data = ""
while game:
    answer = get_answer()
    if cur_game_data["destination_list"][answer-1] == "exit":
        game = False
        break

    cur_game_data = get_data(cur_game_data["destination_list"][answer-1])
    print_text(cur_game_data["string_list"])

print()
print(" C PHINY TMTC ")
print()
exit()
