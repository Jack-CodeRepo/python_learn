#!/usr/bin/env python
# coding: utf-8
# ==================================================================================================


# impossible d'importer
#       from src.mod_a.moda01 import Moda01
#       print(Moda01.name)




# ajouter le chemin du root folder contenant les modules cibles fonctionne
# la presence des __init__.py dans les folders et subfolders ne semble plus etre nécessaire
from os.path import abspath, dirname, join
from sys import path
path.insert(0, abspath(join(dirname(__file__), "../../")))

from src.mod_a.moda01 import Moda01
print(Moda01.name)