#!/usr/bin/env python
# coding: utf-8
# ==================================================================================================

"""


"""

# ==================================================================================================
#   IMPORT STANDARD LIBRARIES AND MODULES
# ==================================================================================================

from os.path import abspath, dirname, join
from sys import path


# add full path poiting to the folder test/../  in path  (aka /path/to/project/folder)
path.insert(0, abspath(join(dirname(__file__), "..")))



# ==================================================================================================
#   IMPORT CUSTOM LIBRARIES AND MODULES
# ==================================================================================================

from src.mod_a.moda01 import Moda01


print(Moda01.name)