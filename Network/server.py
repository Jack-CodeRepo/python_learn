#!/usr/bin/env python
# coding: utf-8
# ==================================================================================================

import http.server
import socketserver

numero_port = 80

# adresse est un tuple contenant le host et le port
# pas la peine de renseigner l'ip host car le server tourne en localhost
adresse = ("", numero_port)
# Variantes fonctionnelles:
# adresse = ("localhost", numero_port)
# adresse = ("127.0.0.1", numero_port)

# instanciation du gestionnaire de requete (ici hhtp)
gestionnaire = http.server.SimpleHTTPRequestHandler

# instanciation du daemon (processus arriere plan)
# 
http_deamon = socketserver.TCPServer(adresse, gestionnaire)


# Demarrage du server
print("[INFO] HTTP Server is started on port {}".format(numero_port))
http_deamon.serve_forever()