#!/usr/bin/env python
# coding: utf-8
# ==================================================================================================

import json

class jsonFile():
    def __init__(self, filePath):
        self._filePath = filePath
        self._data = {}

        self.get_data()

# --------------------------------------------------------------------------------------------------
#	PROPERTIES
# --------------------------------------------------------------------------------------------------

    

# --------------------------------------------------------------------------------------------------
    @property
    def filePath(self):
        return self._filePath

    @filePath.setter
    def filePath(self, value):
        self._filePath = value

# --------------------------------------------------------------------------------------------------
    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, value):
        self._data = value



# --------------------------------------------------------------------------------------------------
#	METHODES
# --------------------------------------------------------------------------------------------------


    def get_data(self):
        with open(self.filePath) as f:
            self.data = json.load(f)

    def write_data(self):
        with open(self.filePath, "w") as f:
            json.dump(self.data, f, indent=4)

    def delete_data(self, dict_k, key):
        del self.data[dict_k][key]
        self.write_data()

    def modify_data(self, dict_k, key, value):
        self.data[dict_k][key] = float(value)
        self.write_data()

    def add_data(self, dict_k, key, value):
        added_data = {key: value}
        self.data[dict_k].update(added_data)
        self.write_data()