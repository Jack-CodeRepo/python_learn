#!/usr/bin/env python
# coding: utf-8
# ==================================================================================================
"""
    Simple budget program

    Usage:
        pendu_main.py [-s | --start]
        pendu_main.py [-h | --help]
        pendu_main.py [--version]

    Options:
        -s  --start     Start the module
        -h  --help      Show this screen
        --version       Show the version

"""

# ==================================================================================================
#   DOCUMENTATION
# ==================================================================================================
#
#   FUNCTIONS:
#       write_json():
#       add_json_element(dict_k, key, value):
#       modify_json_element(dict_k, key, value):
#       delete_json_element(dict_k, key):
#       get_total_inc_exp(inc_exp):
#       key_exists(dict_k, key):
#       set_inc_exp(inc_exp):
#       handle_sub_menu_budget(entry):
#       check_entry_format(entry, num):
# 
#   DISPLAY FUNCTIONS
#       display_add_element():
#       display_modify_element():
#       display_delete_element():
#       display_detailed_inc_exp(inc_exp):
#       display_rad():
#       display_mode_buget_menu():
#
# ==================================================================================================
#   DOCUMENTATION
# ==================================================================================================



# ==================================================================================================
#   IMPORT
# ==================================================================================================

# standard libraries
import docopt
from time import sleep


# custom python files
from .menu_string import *
from .global_func import display_separator, get_path
from .jsonFile import jsonFile



# ==================================================================================================
#   GLOBAL VARIABLES
# ==================================================================================================
root_folder = get_path(__file__)
string_input = ("Please input your choice: ")
string_input_ko = "    INPUT DO NOT RESPECT FORMAT: "

std_time_sleep = 1
short_time_sleep = 0.1



# ==================================================================================================
#   JSON FILE OBJECT
# ==================================================================================================

saveFileJson = jsonFile(root_folder + "/../data/data.json")


# ==================================================================================================
#   FUNCTIONS
# ==================================================================================================


# --------------------------------------------------------------------------------------------------
def add_json_element(dict_k, key, value):
    """
    Adding an element in data.json

    Args:
        dict_k (str):   "expenses" or "incomes"
        key (str):      the minor key to be added into data.json
        value (int):    the value to set the key with
    """
    saveFileJson.add_data(dict_k, key, value)
    print(f"    Adding '{key}: {value}' to data in {dict_k}.\n")


# --------------------------------------------------------------------------------------------------
def modify_json_element(dict_k, key, value):
    """
    Modifying an element in data.json

    Args:
        dict_k (str):   "expenses" or "incomes"
        key (str):      the minor key to be deleted into data.json
        value (int):    the value to update the key with
    """
    saveFileJson.modify_data(dict_k, key, value)
    print(f"    Modifying {key} in data with {value} in {dict_k}.\n")


# --------------------------------------------------------------------------------------------------
def delete_json_element(dict_k, key):
    """
    Deleting an element in data.json

    Args:
        dict_k (str): "expenses" or "incomes"
        key (str): the minor key to be deleted into data.json
    """
    saveFileJson.delete_data(dict_k, key)
    print(f"    Deleting {key} in data from {dict_k}.\n")


# --------------------------------------------------------------------------------------------------
def get_total_inc_exp(inc_exp):
    """
    Calculate the total of the expenses or incomes, depending of argument.

    Args:
        inc_exp (str):  A letter that symbolize the major key to search into the data.json file.
                        "e" for expenses
                        "i" for incomes

    Returns:
        float: Total of either expenses or incomes
    """

    dict_k = set_inc_exp(inc_exp)
    total = float()
    for k in saveFileJson.data[dict_k]:
        total += saveFileJson.data[dict_k][k]
    return total


# --------------------------------------------------------------------------------------------------
def key_exists(dict_k, key):
    """
    Check if the key already exists in data.json

    Args:
        dict_k (str): "expenses" or "incomes"
        key (str): the minor key to look into data.json

    Returns:
        boolean: True if the minor key is found, False if it is not found
    """

    if key in saveFileJson.data[dict_k]:
        return True
    else:
        return False


# --------------------------------------------------------------------------------------------------
def set_inc_exp(inc_exp):
    """
    Returns "expenses" if arg is "e"
    Returns "incomes" if arg is "i"

    Args:
        inc_exp (str):  A letter that symbolize the major key to search into the data.json file.
                        "e" for expenses
                        "i" for incomes
    Returns:
        str: "expenses" or "incomes", depending of the argument
    """

    if inc_exp == "e":
        dict_key = "expenses"
    elif inc_exp == "i":
        dict_key = "incomes"
    return dict_key


# --------------------------------------------------------------------------------------------------
def handle_sub_menu_budget(entry):
    """[summary]

    Args:
        entry ([type]): [description]

    Returns:
        [type]: [description]
    """
    if entry == "1":
        display_detailed_inc_exp("e")
        return True
    elif entry == "2":
        display_detailed_inc_exp("i")
        return True
    else:
        return False

# --------------------------------------------------------------------------------------------------
def check_entry_format(entry, num):
    if entry == "":
        print(string_input_ko + entry)
        return False
    elif not ";" in entry:
        print(string_input_ko + entry)
        return False
    elif len(entry.split(";")) != num:
        print(string_input_ko + entry)
        return False
    else:
        return True



# --------------------------------------------------------------------------------------------------
# DISPLAY FUNCTIONS
# --------------------------------------------------------------------------------------------------

def display_add_element():
    """
    Displays the menu to add one element in data.json.
    Also manages the entry.
    """

    display_active = True
    entry_session = True
 
    while display_active:
        display_separator("=", 50)
        print(menu_bud_add_string)
        if not entry_session:
            break
        else:
            while entry_session:
                entry = input("    Entry: ")
                display_separator("-", 50)

                if handle_sub_menu_budget(entry):
                    break
                elif entry == "0":
                    display_active = False
                    break
                elif not check_entry_format(entry, 3):
                    break
                else:
                    choice = entry.split(";")[0]
                    key = entry.split(";")[1]
                    value = entry.split(";")[2]
                    value = float(value)

                    if "e" not in choice and "i" not in choice:
                        break

                    dict_k = set_inc_exp(choice)
                    
                    if key_exists(dict_k, key):
                        print(f"{key} already exists in {dict_k}")
                        break

                    add_json_element(dict_k, key, value)
                    sleep(std_time_sleep)
                    break


# --------------------------------------------------------------------------------------------------
def display_modify_element():
    """
    Displays the menu to modify one element in data.json.
    Also manages the entry.
    """

    display_active = True
    entry_session = True

    while display_active:
        display_separator("=", 50)
        print(menu_bud_mod_string)
        if not entry_session:
            break
        else:
            while entry_session:
                entry = input("    Entry: ")
                display_separator("-", 50)

                if handle_sub_menu_budget(entry):
                    break
                elif entry == "0":
                    display_active = False
                    break
                elif not check_entry_format(entry, 3):
                    break
                else:
                    choice = entry.split(";")[0]
                    key = entry.split(";")[1]
                    value = entry.split(";")[2]
                    value = float(value)

                    if "e" not in choice and "i" not in choice:
                        print(string_input_ko + entry)
                        break
                    
                    dict_k = set_inc_exp(choice)

                    if not key_exists(dict_k, key):
                        print(f"    {key} do not exists in {dict_k}")
                        break

                    modify_json_element(dict_k, key, value)
                    break


# --------------------------------------------------------------------------------------------------
def display_delete_element():
    """
    Displays the menu to delete one or several elements in data.json.
    Also manages the entry.
    """

    display_active = True
    entry_session = True
    
    while display_active:
        display_separator("=", 50)
        print(menu_bud_del_string)
        if not entry_session:
            break
        else:
            while entry_session:
                entry = input("    Entry: ")
                display_separator("-", 50)
                if handle_sub_menu_budget(entry):
                    break
                elif entry == "0":
                    display_active = False
                    break
                elif not check_entry_format(entry, 2):
                    break
                else:
                    entry_split = entry.split(";")
                    choice = entry_split[0]
                    keys = entry_split[1]
            
                    if "e" not in choice and "i" not in choice:
                        print(string_input_ko + entry)
                        break

                    dict_k = set_inc_exp(choice)

                    for key in keys.split(","):
                        if not key_exists(dict_k, key):
                            print(f"    {key} do not exists in {dict_k}")
                            break

                        delete_json_element(dict_k, key)
                        sleep(short_time_sleep)
                    break


# --------------------------------------------------------------------------------------------------
def display_detailed_inc_exp(inc_exp):
    """
    Displays detailed expenses or incomes

    Args:
        inc_exp (str):  A letter that symbolize the major key to search into the data.json file.
                        "e" for expenses
                        "i" for incomes
    """
    display_separator("=", 100)
    dict_k = set_inc_exp(inc_exp)
    print(f"\n    List of {dict_k}: ")
    display_separator("-", 50)
    for k in saveFileJson.data[dict_k]:
        print("    " + k + ": " + str(saveFileJson.data[dict_k][k]))
        sleep(short_time_sleep)
    print("\n")


# --------------------------------------------------------------------------------------------------
def display_rad():
    """
    Diplays:
        - Total of expenses
        - Total of incomes
        - The difference between incomes and expenses
    """
    display_separator("=", 100)

    total_exp = float(get_total_inc_exp("e"))
    total_inc = float(get_total_inc_exp("i"))
    remain = float(total_inc - total_exp)

    string_tot_exp = f"    Total of expenses: {total_exp}"
    string_tot_inc = f"    Total of incomes: {total_inc}"
    string_rad = f"    Remains: {remain}"

    print("\n    Short Budget is:")
    display_separator("-", 50)
    print(f"{string_tot_inc} \n{string_tot_exp} \n{string_rad}\n")
    sleep(short_time_sleep)


# --------------------------------------------------------------------------------------------------
def display_buget_main_menu():
    """
    Displays the main menu for budget mode.
    """

    display_active = True

    while display_active:
        display_separator("=", 100)
        print(menu_bud_main_string)
        display_separator("=", 100)
        answer = input(string_input)

        if answer == "1":
            display_rad()
        if answer == "2":
            display_detailed_inc_exp("e")
        if answer == "3":
            display_detailed_inc_exp("i")
        if answer == "4":
            display_add_element()
        if answer == "5":
            display_modify_element()
        if answer =="6":
            display_delete_element()
        if answer == "0":
            display_active = False
            break
        else:
            pass



# ==================================================================================================
#   SCRIPT
# ==================================================================================================

if __name__ == "__main__":
    args = docopt.docopt(__doc__, version="1.0", options_first=True)

    if args["-s"] or args["--start"]:
        display_buget_main_menu()
    else:
        print("Please type 'python mode_budget.py -h' in the terminal to display the help page.")
        exit()
