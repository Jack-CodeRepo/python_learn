#!/usr/bin/env python
# coding: utf-8
# ==================================================================================================



# ==================================================================================================
#   BUDGET MENU
# ==================================================================================================

menu_bud_add_string = """
ADD ELEMENT
Input the name of the element and its value.
1) Show detailed expenses
2) Show detailed incomes
0) Cancel action
--------------------------------------------------------
Format for income is:   i;name;value
Format for expense is:  e;name;value
"""



menu_bud_mod_string = """
MODIFY ELEMENT
Input the name of the element and its value.
1) Show detailed expenses
2) Show detailed incomes
0) Cancel action
--------------------------------------------------------
Format for income is:   i;name;value
Format for expense is:  e;name;value
"""





menu_bud_del_string = """
DELETE ELEMENT
Input the name of the element to delete.
1) Show detailed expenses
2) Show detailed incomes
0) Cancel action
--------------------------------------------------------
Deleting only one element:
Format for income is:   i;element01,
Format for expense is:  e;element01,

Deleting more than one element:
Format for income is:   i;element01,element02 ...
Format for expense is:  e;element01,element02 ...
"""




menu_bud_main_string = """
1) Show Short Budget
2) Show detailed expenses
3) Show detailed incomes
4) Add budget element
5) Modify budget element
6) Delete budget element(s)
------------------------------------
0) Return to previous menu
"""






# ==================================================================================================
#   COMPTA MENU
# ==================================================================================================

menu_cpt_show_balance = """
1) Show simple Balance
2) Show setailed Balance
------------------------------------
0) Return to previous menu
"""

menu_cpt_input_element = """
INPUT ELEMENT
Input the element to add.
1) Show detailed posts
0) Cancel action
--------------------------------------------------------
Format for  is:   myPostNumber;myLabel;myAmount
"""

menu_cpt_modify_element = """
1) Show simple Balance
2) Show detailed Balance
------------------------------------
0) Return to previous menu
"""

menu_cpt_delete_element = """
1) Show simple Balance
2) Show setailed Balance
------------------------------------
0) Return to previous menu
"""

menu_cpt_main_string = """
1) Show Balance
2) Input a new element
3) Modify element
4) Delete element
------------------------------------
0) Return to previous menu
"""
