#!/usr/bin/env python
# coding: utf-8
# ==================================================================================================

"""
    descriptionDuScript

    Usage:
        mode_compta.py    [-h | --help]
        mode_compta.py    [--version]
        mode_compta.py    [-a <ligne>]
        mode_compta.py    [--view-posts]

    Options:
        -a <ligne>      Ajoute une ligne dans le fichier de données.
                        Syntaxe de <ligne>: posteBudgetaire,label,montant
        --view-posts    Voir la liste des postes
        -h  --help      Show this screen
        --version       Show the version
"""

# ==================================================================================================
#	IMPORT
# ==================================================================================================

# standard libraries
from docopt import docopt
from calendar import month_name
from re import split, sub
# custom python files
from .menu_string import *
from .global_func import display_separator, get_path


# ==================================================================================================
#	VARIABLES GLOBALES
# ==================================================================================================
versionNum = "1.0"
versionString = f"{__file__} {versionNum}"

string_input = ("Please input your choice: ")
string_input_ko = "    INPUT DO NOT RESPECT FORMAT: "

root_folder = get_path(__file__) + "/.."



# ==================================================================================================
#   DATA
# ==================================================================================================

headLine = "posteBudgetaire;libelle;soldeBanque;alimentation;loisir;vetement;telInternet;energie;transport;sante;divers;"
csvfile = f"{root_folder}/data/data.csv"

def get_file_data(file=csvfile):
    with open(file, "r") as f:
        data = f.readlines()
    return data



# ==================================================================================================
#	FONCTIONS
# ==================================================================================================


# --------------------------------------------------------------------------------------------------
def showColname():
    ListHeadLine = str(get_file_data()[0]).split(";")
    display = []
    for idx, elem in enumerate(ListHeadLine):
        if idx <= 2:
            continue
        if ListHeadLine[idx] == "\n":
            break
        if ListHeadLine[idx+1] == "\n":
            display.append(f"{idx}: {ListHeadLine[idx]} //")
            break
        if str(idx) not in str(display):
            display.append(f"{idx}: {ListHeadLine[idx]} // {idx+1}: {ListHeadLine[idx+1]}")

    for elem in display:
        print(elem)



# --------------------------------------------------------------------------------------------------
def show_full_data(file=csvfile):
    """Affiche toutes les premieres colonnes

    Args:
        file (file, optional): a un fichier csv avec separateur ";". Defaults to csvfile.
    """

    with open(file, 'r') as f:
        dataLines = f.readlines()

    maximum = max([len(i) for i in dataLines[0].split(";") ]) + 1

    lignelist = list()
    for ligne in dataLines:
        lignelist.append(align_ligne(ligne, maximum))

    for ligne in lignelist:
        print(sub("\n","","".join(ligne)))



# --------------------------------------------------------------------------------------------------
def show_simple_data(file=csvfile):
    """Affiche les trois premieres colonnes

    Args:
        file (file, optional): a un fichier csv avec separateur ";". Defaults to csvfile.
    """

    with open(file, 'r') as f:
        dataLines = f.readlines()
    lignelist = list()

    maximum = max([len(i) for i in dataLines[0].split(";")[:3] ]) + 5
    for ligne in dataLines:
        lignelist.append(align_ligne(ligne, maximum, maxCol=3))

    for ligne in lignelist:
        print(sub("\n","","".join(ligne)))



# --------------------------------------------------------------------------------------------------
def align_ligne(ligne: str, maxLen, maxCol=None, mySeparator=";"):
    if not maxCol:
        maxCol = -1
    colList = list()
    for col in ligne.split(mySeparator)[:maxCol]:
        if col == "":
            colList.append(maxLen*" ")
        else:
            for i in range(maxLen):
                if len(col) != maxLen:
                    col += " "
                elif len(col) == maxLen:
                    colList.append(col)
                    break
    return colList



# --------------------------------------------------------------------------------------------------
def appendRow(myData):
    ListHeadLine = str(get_file_data()[0]).split(";")

    post = ListHeadLine[int(myData.split(";")[0])]
    label = myData.split(";")[1]
    amount = str(myData.split(";")[2])
    ligne = generateLine(post,label,amount)
    writeFile(csvfile, ligne)



# --------------------------------------------------------------------------------------------------
def writeFile(fichier, dataString):
    _f = open(fichier, 'a+')
    _f.write(dataString + '\n')
    _f.close()



# --------------------------------------------------------------------------------------------------
def generateLine(myPoste, myLabel, myAmount):
    myIdx = int()
    comaSep = str()
    ListHeadLine = str(get_file_data()[0]).split(";")

    for elem in ListHeadLine:
        if myPoste not in ListHeadLine:
            print(f"'{myPoste}' non reconnu en tant que poste bugetaire.")
            break
        if myPoste == elem:
            myIdx = ListHeadLine.index(myPoste)
        string = f"{myPoste};{myLabel};{myAmount}"
    for i in range(myIdx-2):
        comaSep += ";"
    tmpStr = string+comaSep+myAmount
    tmp = tmpStr.split(";")
    while len(tmp) != len(ListHeadLine):
        tmpStr += ";"
        tmp = tmpStr.split(";")
    return tmpStr



# ==================================================================================================
#   DISPLAY
# ==================================================================================================


# --------------------------------------------------------------------------------------------------
def display_balance():
    depenses = 0
    revenus = 0
    for ligne in get_file_data()[1:]:
        nombre = float(ligne.split(";")[2])
        if nombre < 0:
            depenses += nombre
        else:
            revenus += nombre
    display_separator("-", 50)
    print(f"Depenses: {depenses}")
    print(f"Revenus: {revenus}")
    print(f"Solde: {revenus + depenses}")

# --------------------------------------------------------------------------------------------------
def display_input_element():

    display_active = True

    while display_active:
        entry_session = True
        display_separator("=", 50)
        print(menu_cpt_input_element)
        while entry_session:
            entry = input("    Entry: ")
            display_separator("-", 50)
            if entry == "0":
                display_active = False
                break
            elif entry == "1":
                showColname()
            elif entry == "":
                print(string_input_ko + entry)
                entry_session = False
                break
            elif not ";" in entry:
                print(string_input_ko + entry)
                entry_session = False
                break
            elif len(entry.split(";")) != 3:
                print(string_input_ko + entry)
                entry_session = False
                break

            else:
                appendRow(entry)
                break



# --------------------------------------------------------------------------------------------------
def display_modify_element():
    display_active = True

    while display_active:
        entry_session = True
        display_separator("=", 50)
        print(menu_cpt_modify_element)
        while entry_session:
            entry = input("    Entry: ")
            display_separator("-", 50)
            if entry == "0":
                display_active = False
                break
            elif entry == "1":
                show_simple_data()
            elif entry == "2":
                show_full_data()
            elif entry == "":
                print(string_input_ko + entry)
                entry_session = False
                break
            elif not ";" in entry:
                print(string_input_ko + entry)
                entry_session = False
                break
            elif len(entry.split(";")) != 3:
                print(string_input_ko + entry)
                entry_session = False
                break

            else:
                appendRow(entry)
                break


# --------------------------------------------------------------------------------------------------
def display_delete_element():
    print("delete")



# --------------------------------------------------------------------------------------------------
def display_andReturn_select_month():
    pass
    # answer = (string_input)
    # print(str(month_name[1:]).lower())



# --------------------------------------------------------------------------------------------------
def display_compta_main_menu():
    """
    Displays the main menu for compta mode.
    """

    display_active = True

    while display_active:
        display_separator("=", 100)
        print(menu_cpt_main_string)
        display_separator("=", 100)
        answer = input(string_input)

        if answer == "1":
            display_balance()
        if answer == "2":
            display_input_element()
        if answer == "3":
            display_modify_element()
        if answer == "4":
            display_delete_element()
        if answer == "0":
            display_active = False
            break
        else:
            pass



# ==================================================================================================
#	SCRIPT
# ==================================================================================================

if __name__ == "__main__":
    arguments = docopt(__doc__, version=versionString, options_first=True)

    if arguments["-a"]:
        print(arguments["-a"])
        # ecrire_dans_donnes(arguments["-a"])

    if arguments["--view-posts"]:
        showColname()

    # else:
        # print("Please type 'python mode_compta.py -h' in the terminal to display the help page.")
        # exit()
