#!/usr/bin/env python
# coding: utf-8
# ==================================================================================================

import readline
from pathlib import Path


def display_separator(char, num):
    """[summary]

    Args:
        char ([type]): [description]
        num ([type]): [description]
    """
    separator = ""
    for n in range(num):
        separator += char
    print(separator)



def get_path(fichier):
    """
    Retourne le chemin du fichier donné en argument
    A été codé pour un usage tel que:
    variable = getpath(__file__)

    Args:
        fichier (str): le chemin absolu d'un fichier.
            exemple: /mon/chemin/vers/le/fichier.extension

    Returns:
        str: le chemin aboslu du fichier, sans le nom du fichier
            exemple: /mon/chemin/vers/le
    """
    return str(Path(fichier).parent)