#!/usr/bin/env python
# coding: utf-8
# ==================================================================================================



# ==================================================================================================
#   IMPORT
# ==================================================================================================

import time

from src.mode_budget import display_buget_main_menu, display_separator
from src.mode_compta import display_compta_main_menu


# ==================================================================================================
#   GLOBAL VARIABLES
# ==================================================================================================

string_input = ("Please input your choice: ")
string_input_ko = "    INPUT DO NOT RESPECT FORMAT: "

std_time_sleep = 1
short_time_sleep = 0.1

# ==================================================================================================
#   FUNCTIONS
# ==================================================================================================

def display_menu():
    display_active = True
    welcome_text="""
    Welcome in compta_py program.
    It allows to do basic domestic accountacy.
    """
    main_menu_string = """
    1) Mode: budget
    2) Mode: accountancy
    ------------------------------------
    0) Exit
    """
    display_separator("=", 100)
    print(welcome_text)
    while display_active:
        display_separator("=", 100)
        print(main_menu_string)
        display_separator("=", 100)
        answer = input(string_input)

        if answer == "1":
            display_buget_main_menu()
        if answer == "2":
            display_compta_main_menu()
        if answer == "0":
            time.sleep(std_time_sleep)
            display_separator("=", 100)
            print("EXITING PROGRAM")
            display_separator("=", 100)
            time.sleep(std_time_sleep)
            exit()
        else:
            pass


if __name__ == "__main__":
    display_menu()
else:
    exit()
