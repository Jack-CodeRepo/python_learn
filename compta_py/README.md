# compta_py
Programme de comptabilité domestique de type recettes/dépenses.
</br> Les éléments retenus les recettes et dépenses récurrentes.
</br> Possibilité de modifier les données directement via le fichier data.json

Usage:
</br>   - sh start.sh (le shebang est: /bin/bash => OS avec le shell bash)
</br>   - python main.py (tout les OS)

Fonctionalité principales:
</br>   - Afficher le reste à dépenser (RAD)
</br>   - Afficher les dépenses
</br>   - Afficher les revenus
</br>   - Ajouter un élément
</br>   - Supprimer un élément
</br>   - Modifier un élément