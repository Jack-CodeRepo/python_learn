#!/usr/bin/env python
# coding: utf-8
# ==================================================================================================

"""
    Merge csv files contained in a folder
    default: The folder containing this python script will be considered as the folder containing the csv files
                Read "Usage" and "Options" documentation for detailed informations


    Usage:
        merge.py    [--path <myPath>] [--pattern <pattern>]
        merge.py    [-h | --help]
        merge.py    [--version]

    Options:
        --path <myPath>         Specify the path to the folder containing the csv file
        --pattern <pattern>     Specify the pattern to detect in the file name of elligible csv file
        -h  --help              Show this screen
        --version               Show the version
"""


# ==================================================================================================
#	IMPORT
# ==================================================================================================
from pathlib import Path
from re import sub
from docopt import docopt
from datetime import date

# ==================================================================================================
#	VARIABLES GLOBALES
# ==================================================================================================
versionNum = "1.0"
versionFileName = Path(__file__).name
versionString = f"{versionFileName} '--jack--' {versionNum}"
colNames = "parenté;prenom;mois;jour;affiliation"


# ==================================================================================================
#	FONCTIONS
# ==================================================================================================

def writeFile(fichier, dataList, firstLine=None):
    """[summary]

    Args:
        fichier ([type]): [description]
        dataList ([type]): [description]
        firstLine ([type], optional): [description]. Defaults to None.
    """
    _f = open(fichier, 'a+')
    if firstLine:
        _f.write(firstLine + '\n')
    for element in dataList:
        _f.write(element + '\n')
    _f.close()



# ==================================================================================================
#	SCRIPT
# ==================================================================================================
if __name__ == "__main__":

    tmpData = list()
    listFile = list()
    outData = list()


    # Parsing arguments and attributing values from them
    arguments = docopt(__doc__, version=versionString, options_first=True)
    actualPath = ""
    if arguments["--path"]:
        actualPath = Path(arguments["--path"])
    else:
        actualPath = Path(__file__).parent

    if arguments["--pattern"]:
        pattern = arguments["--pattern"]
    else:
        pattern = "ouatéveur"


    # check the folder 
    # if it exists,   if it is a folder,   if it can be iterated
    if not actualPath.exists():
        print(f"[ERROR] {actualPath} does not exists")
        exit()
    if not actualPath.is_dir():
        print(f"[ERROR] {actualPath} is not a directory")
        exit()
    if not actualPath.iterdir():
        print(f"[ERROR] {actualPath} does not contain any file")
        exit()


    tmpData = list()    # list of lists // one element is a list of lines, representing the full content of a csv file
    fileList = list()
    csvFileCounter = int()

    # parsing all files and adding all datafile in tmp list from elligible file
    for doc in actualPath.iterdir():
        if Path(doc).suffix == ".csv":
            csvFileCounter += 1
            if pattern in Path(doc).name:
                with open(str(Path(doc))) as f:
                    tmpData.append(f.readlines())
                    print(f"[INFO] the file {doc} will be merged")

    # exiting program if no file found
    if csvFileCounter == 0:
        print(f"[ERROR] no elligible files detected in {actualPath}")
        exit()


    for fileData in tmpData:
        for ligne in fileData[1:]:
            outData.append(sub("\n", "", ligne))


    # generates a date that will be used in the output file name
    dateSignature = date.today()
    year, month, day = str(dateSignature).split("-")[0], str(dateSignature).split("-")[1], str(dateSignature).split("-")[2]
    outputFile = str(actualPath) + f"/{year}{month}{day}_output.csv"
    

    # writing the file
    writeFile(outputFile, outData, firstLine=colNames)


else:
    print(f"{__file__} not launched as main") 
