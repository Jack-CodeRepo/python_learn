#!/usr/bin/env python
# coding: utf-8
# ==================================================================================================

"""
    Linux / shell (bash, zh, ...)usage only.

    Usage:
        git_pull.py    (--myrepo-folder <myRepoFolder>)     [--branch-name <branchName>] 
        git_pull.py    (--repos-path <reposPath>)           [--branch-name <branchName>] 
        git_pull.py    [-h | --help]
        git_pull.py    [--version]

    Options:
        --myrepo-folder <myRepoFolder>          Specify path to specific repository folder to pull
        --repos-path <reposPath>                Specify absolute path to a folder containing repositories to pull
        --branch-name <branchName>              Specify branch to pull (default will be 'main')
        -h  --help                              Show this screen
        --version                               Show the version
"""



# ==================================================================================================
#	IMPORT
# ==================================================================================================

from docopt import docopt

from os import system, chdir
from subprocess import getoutput
from pathlib import Path



# ==================================================================================================
#	VARIABLES GLOBALES
# ==================================================================================================

versionNum = "1.0"
versionString = f"{__file__} {versionNum}"
arguments = docopt(__doc__, version=versionString, options_first=True)

TAG_ERROR = "[ERROR]"



# ==================================================================================================
#	ARGUMENTS MANAGEMENT
# ==================================================================================================

argRepoFolder = ""
argRepoPath = ""
argBranchName = ""

if not arguments["--myrepo-folder"] and not arguments["--repos-path"]:
    print("Please do \"git_pull -h\" and read the documentation")
    exit()

if arguments["--myrepo-folder"]:
    argRepoFolder = str(arguments["--myrepo-folder"])

if arguments["--repos-path"]:
    argRepoPath = str(arguments["--repos-path"])

if arguments["--branch-name"]:
    argBranchName = str(arguments["--branch-name"])
else:
    argBranchName = "main"



# ==================================================================================================
#	FONCTIONS
# ==================================================================================================


# --------------------------------------------------------------------------------------------------
def folder_contain_git_repo(folder):
    """Verify if <folder> is a git repository

    Args:
        folder (str): absolute path to a folder

    Returns:
        [bool]: True if <folder> is a git repository, False if <folder> is not a git repository
    """
    chdir(str(folder))
    output = getoutput("ls -la | grep git")
    if ".git" in output:
        return True
    else:
        return False



# --------------------------------------------------------------------------------------------------
def check_folder(chemin):
    error = False
    repoPath = Path(chemin)
    if not repoPath.exists():
        print(f"{TAG_ERROR} Path {chemin} does not exists.")
        error = True
    elif not repoPath.is_dir():
        print(f"{TAG_ERROR} Path {chemin} is a file.\n{TAG_ERROR} Please input a path pointing to folder.")
        error = True
    elif not any(repoPath.iterdir()):    
        print(f"{TAG_ERROR} Repository {chemin} is empty.")
        error = True

    if error:
        exit()
    else:
        return repoPath



# --------------------------------------------------------------------------------------------------
def list_repos_in_folder(chemin):
    """List all git repositories contained in <chemin>

    Args:
        chemin (str): absolute path to a folder containing git repositories

    Returns:
        [list]: list of git repositories
    """
    repoPath = check_folder(chemin)
    reposList = list()
    for fichier in repoPath.iterdir():
        if fichier.is_file():
            continue
        if folder_contain_git_repo(fichier):
            reposList.append(str(fichier))
    if not reposList:
        print(f"{TAG_ERROR} No git repositories detected in {repoPath}. \n{TAG_ERROR} Please input appropriate path.")
    return reposList



# --------------------------------------------------------------------------------------------------
def pull_one_repo(repoPath, branchName: str = "main"):
    """Pull one repository

    Args:
        repoPath    (str):  path to the folder containing the repository
        branchName  (str):  name of the branch to pull
    """
    chdir(repoPath)
    branch = getoutput("git branch | grep \* | cut -d ' ' -f2")
    print("===============================================================================")
    print(f"{repoPath} :: current branch => {branch}")
    print("===============================================================================")
    system(f"git checkout {branchName}")
    system("git pull")
    system(f"git checkout {branch}")
    print()



# ==================================================================================================
#	SCRIPT
# ==================================================================================================


if __name__ == "__main__":
    if argRepoPath:
        myRepos = list_repos_in_folder(argRepoPath)
        for repo in myRepos:
            pull_one_repo(repo,argBranchName)




    if argRepoFolder:
        myFolder = check_folder(argRepoFolder)
        if folder_contain_git_repo(myFolder):
            pull_one_repo(argRepoFolder, argBranchName)



