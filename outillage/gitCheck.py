#!/usr/bin/env python
# coding: utf-8
# ==================================================================================================

"""
    descriptionDuScript

    Usage:
        nomduScript.py    [-h | --help]
        nomduScript.py    [--version]

    Options:
        -h  --help    Show this screen
        --version     Show the version
"""

# ==================================================================================================
#	IMPORT
# ==================================================================================================
from docopt import docopt
from subprocess import getoutput
from os import chdir, environ
from pathlib import Path

# ==================================================================================================
#	VARIABLES GLOBALES
# ==================================================================================================
versionNum = "1.0"
versionString = f"{__file__} {versionNum}"

envVar = {"HOME": environ['HOME']}


progdev = Path(f"{envVar['HOME']}/Programmes_dev/")
gitStatus = "git status"


# ==================================================================================================
#	FONCTIONS
# ==================================================================================================



# ==================================================================================================
#	SCRIPT
# ==================================================================================================

if __name__ == "__main__":
    arguments = docopt(__doc__, version=versionString, options_first=True)
    pass



for folder in progdev.iterdir():
    if not folder.is_file():
        chdir(folder)
        output = getoutput(gitStatus)
        if envVar['HOME'] in str(folder):
            folder = str(folder).strip(envVar['HOME'])
        print(f"Dépot: \033[32m{folder}\033[0;0m \nSortie console '{gitStatus}': \n\033[95m{output}\033[0;0m\n")
